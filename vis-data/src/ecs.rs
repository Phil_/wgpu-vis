use legion::{IntoQuery, Resources, World};

use crate::geo::{Coord, LambertConformal, Projection};
use ndarray::Axis;
use ordered_float::OrderedFloat;
use std::convert::TryInto;
use vis_core::state::ToVertexVec;
use vis_core::state::Vertex;

// component
#[derive(Clone, Debug, Copy, PartialEq)]
pub struct Data {
    co: f32,
    h2: f32,
}

impl Default for Data {
    fn default() -> Self {
        Data { co: 0., h2: 0. }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position {
    pub coord: Coord,
    layer: u8,
    timestep: u8,
}

// resource
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct CenterPoint {
    lon: f32,
    lat: f32,
}

impl Default for CenterPoint {
    fn default() -> Self {
        CenterPoint { lat: 0., lon: 0. }
    }
}

pub struct Ecs {
    pub world: World,
    pub resources: Resources,
}

impl Default for Ecs {
    fn default() -> Self {
        Self {
            world: World::default(),
            resources: Resources::default(),
        }
    }
}

// simple test: just read a single var to the world
impl Ecs {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn append_to_world(&mut self, path: &str, variable: &str) {
        let file = netcdf::open(path).unwrap();

        // extract lat + lon
        let grid_lat = &file
            .variable("lat")
            .expect("no such variable: lat")
            .values::<f32>(None, None)
            .unwrap();
        let grid_lon = &file
            .variable("lon")
            .expect("no such variable: lon")
            .values::<f32>(None, None)
            .unwrap();

        // add center point to resources
        let cenlon = match file.attribute("cenlon").unwrap().value().unwrap() {
            netcdf::AttrValue::Float(val) => val,
            _ => 0.0,
        };
        let cenlat = match file.attribute("cenlat").unwrap().value().unwrap() {
            netcdf::AttrValue::Float(val) => val,
            _ => 0.0,
        };
        let center_point = CenterPoint {
            lon: cenlon,
            lat: cenlat,
        };
        self.resources.insert(center_point);

        // add projection to resources
        let standard_parallel = match file
            .variable("Lambert_conformal")
            .unwrap()
            .attribute("standard_parallel")
            .unwrap()
            .value()
            .unwrap()
        {
            netcdf::AttrValue::Floats(val) => val.try_into().unwrap(),
            _ => [0f32, 0f32],
        };
        let long_central_meridian = match file
            .variable("Lambert_conformal")
            .unwrap()
            .attribute("longitude_of_central_meridian")
            .unwrap()
            .value()
            .unwrap()
        {
            netcdf::AttrValue::Float(val) => val,
            _ => 0.0,
        };
        let lat_project_orig = match file
            .variable("Lambert_conformal")
            .unwrap()
            .attribute("latitude_of_projection_origin")
            .unwrap()
            .value()
            .unwrap()
        {
            netcdf::AttrValue::Float(val) => val,
            _ => 0.0,
        };
        self.resources.insert(Box::new(LambertConformal::new(
            standard_parallel,
            long_central_meridian,
            lat_project_orig,
        )) as Box<dyn Projection>);

        // read the specified var
        let var = &file
            .variable(variable)
            .unwrap_or_else(|| panic!("no such variable: {}", variable));
        let vals = var.values::<f32>(None, None).unwrap();

        let h0 = vals.index_axis(Axis(0), 3);

        let points: Vec<(Position, Data)> = h0
            // iter over pressure layers
            .outer_iter()
            .enumerate()
            .flat_map(|(layer, grid)| {
                grid
                    // iter over grid coordinates
                    .iter()
                    .zip(grid_lat.iter().zip(grid_lon.iter()))
                    .map(|(&val, (&lat, &lon))| {
                        (
                            Position {
                                coord: Coord { lat, lon },
                                layer: layer as u8,
                                timestep: 0_u8,
                            },
                            Data {
                                co: val,
                                ..Default::default()
                            },
                        )
                    })
                    .collect::<Vec<(Position, Data)>>()
            })
            .collect();
        self.world.extend(points);
    }
}

impl ToVertexVec for Ecs {
    fn to_vertex_vec(&self) -> Vec<Vertex> {
        // min, max, colorscaler for CO
        let mut min_max_query = <&Data>::query();
        let max = min_max_query
            .iter(&self.world)
            .max_by_key(|&val| OrderedFloat(val.co))
            .unwrap()
            .co;
        let min = min_max_query
            .iter(&self.world)
            .min_by_key(|&val| OrderedFloat(val.co))
            .unwrap()
            .co;
        let color = |x: f32| (x - min) / (max - min);

        let mut query = <(&Position, &Data)>::query();
        query
            .iter(&self.world)
            .map(|(&position, &data)| {
                Vertex::new(
                    position.coord.lon,
                    position.coord.lat,
                    position.layer as f32 * 100.,
                    color(data.co),
                )
            })
            .collect()
    }
}
