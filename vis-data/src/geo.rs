use std::f32::consts::FRAC_PI_4;
use std::f32::consts::PI;

pub fn cot(x: f32) -> f32 {
    x.cos() / x.sin()
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Coord {
    pub lat: f32,
    pub lon: f32,
}

pub trait Projection {
    fn transform(&self, input: &Coord) -> Coord;
}

const R: f32 = 6371000.;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct LambertConformal {
    standard_parallel: [f32; 2],
    long_central_meridian: f32,
    lat_project_orig: f32,
}

impl LambertConformal {
    pub fn new(
        standard_parallel: [f32; 2],
        long_central_meridian: f32,
        lat_project_orig: f32,
    ) -> Self {
        Self {
            standard_parallel,
            long_central_meridian,
            lat_project_orig,
        }
    }
}

impl Projection for LambertConformal {
    fn transform(&self, input: &Coord) -> Coord {
        let deg = |x: f32| x * PI / 180.;

        let phi1 = deg(self.standard_parallel[0]);
        let phi2 = deg(self.standard_parallel[1]);
        let lat_project_orig = deg(self.lat_project_orig);
        let long_central_meridian = deg(self.long_central_meridian);

        let lat = deg(input.lat);
        let lon = deg(input.lon);

        let n_upper = (phi1.cos() / phi2.cos()).ln();
        let n_lower = ((FRAC_PI_4 + 0.5 * phi2).tan() * cot(FRAC_PI_4 + 0.5 * phi1)).ln();
        let n = n_upper / n_lower;

        let f = phi1.cos() * (FRAC_PI_4 + 0.5 * phi1).tan().powf(n) / n;
        let rho = R * f * cot(FRAC_PI_4 + 0.5 * lat).powf(n);
        let rho0 = R * f * cot(FRAC_PI_4 + 0.5 * lat_project_orig).powf(n);

        let x = rho * (n * (lon - long_central_meridian)).sin();
        let y = rho0 - rho * (n * (lon - long_central_meridian)).cos();

        dbg!(phi1);
        dbg!(phi2);
        dbg!(lat_project_orig);
        dbg!(long_central_meridian);
        dbg!(lat);
        dbg!(lon);

        Coord { lon: x, lat: y }
    }
}
