{
  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
  };

  outputs = {self, fenix, flake-utils, naersk, nixpkgs, ... }:
  flake-utils.lib.eachDefaultSystem(system: let
    pname = "vis";

    # see https://github.com/NixOS/nixpkgs/pull/106085
    validation-layer-overlay = final: prev: {
      vulkan-validation-layers = prev.vulkan-validation-layers.overrideAttrs(old: {
        setupHook = pkgs.writeText "setup-hook" "";
      });
    };

    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        fenix.overlay
        validation-layer-overlay
      ];
    };

    rustEnv = (pkgs.rust-nightly.latest.withComponents [
      "cargo"
      "clippy-preview"
      "rust-src"
      "rust-std"
      "rustc"
      "rustfmt-preview"
    ]);

    vulkan-libs = with pkgs; [
      vulkan-tools
      vulkan-loader
      vulkan-headers
      vulkan-tools-lunarg
    ];

    naersk-lib = naersk.lib."${system}".override {
      cargo = rustEnv;
      rustc = rustEnv;
    };

  in rec {
    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [
        rustEnv
        renderdoc

        glfw
        glm
        glslang
        shaderc

        pkg-config
        file
        freetype
        expat

        git
        pre-commit
      ] ++ vulkan-libs;

      shellHook = ''
        export LD_LIBRARY_PATH="${pkgs.wayland}/lib/:${pkgs.vulkan-loader}/lib/:${pkgs.libxkbcommon}/lib:$LD_LIBRARY_PATH"
        export SHADERC_LIB_DIR='${pkgs.shaderc.lib}/lib'
      '';
    };

    defaultPackage = naersk-lib.buildPackage rec {
      inherit pname;
      root = ./.;

      release = false;

      nativeBuildInputs = with pkgs; [
        cmake
        git
        tree
        pkg-config
      ];

      buildInputs = with pkgs; [
        shaderc
        hdf5
        file
        freetype
        expat
      ];

      override = _: {
        preBuild = ''
          export SHADERC_LIB_DIR='${pkgs.shaderc.lib}/lib'
        '';
      };

      overrideMain = _: {
        postInstall = ''
          patchelf --add-needed ${pkgs.libxkbcommon}/lib/libxkbcommon.so.0 $out/bin/${pname}
          patchelf --add-needed ${pkgs.wayland}/lib/libwayland-client.so.0 $out/bin/${pname}
          patchelf --add-needed ${pkgs.vulkan-loader}/lib/libvulkan.so.1 $out/bin/${pname}

          tree
        '';
      };
    };

    defaultApp = flake-utils.lib.mkApp {
      drv = defaultPackage;
    };
  });
}
