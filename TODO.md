app.rs
- move most parts of the event loop to vis-core
- flexibility to use the app without a window attached -> render to file(s)
- keyboard mappings?
- Dear ImGui gui


shader.vert:
- move map transformation to compute shader
- store map tranformation parameters in uniform buffer

ecs.rs:
- (variable in ECS struct / trait method) to select which var to visualize
