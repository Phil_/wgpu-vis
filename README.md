# Vis

*Vis* is a WIP 3D generic data renderer/visualizer, specifically targeted towards weather and geolocation data in grid layouts.

Key features:
- builds on [webgpu](https://www.w3.org/community/gpu/)
- Modular data loading system, other file formats are easy to implement

Planned features:
- Modular rendering backend, ability to render to screen or file(s)
- Simple UI based on Dear ImGui
- extensible data transformation pipelines
- animated data / visualization of the time component

See also: [TODO.md](./TODO.md).

## Gallery

![frontal view onto some weather data](./gallery/frontal.png)

![moving the camera to the left](./gallery/perspective.png)
