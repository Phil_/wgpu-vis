use crossterm::{cursor, terminal, QueueableCommand};
use futures::executor::block_on;
use std::io::{stdout, Write};
use vis_core::state::State;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
};

use vis_data::ecs::Ecs;

pub struct App {
    // window options, update methods as Fn, ...
    frames: i16,
    timestamp: std::time::Instant,
    ecs: Ecs,
}

impl App {
    pub fn new() -> App {
        Self {
            frames: 0,
            timestamp: std::time::Instant::now(),
            ecs: Ecs::new(),
        }
    }

    fn update_fps_ticker(&mut self, now: std::time::Instant) {
        self.frames += 1;
        if now - self.timestamp >= std::time::Duration::from_secs(1) {
            let mut stdout = stdout();
            stdout.queue(cursor::SavePosition).unwrap();
            stdout
                .queue(terminal::Clear(terminal::ClearType::CurrentLine))
                .unwrap();
            stdout
                .write_all(format!("fps: {}", self.frames).as_bytes())
                .unwrap();
            stdout.queue(cursor::RestorePosition).unwrap();
            stdout.flush().unwrap();

            self.timestamp = std::time::Instant::now();
            self.frames = 0;
        }
    }

    pub fn load_file(&mut self, filename: &str, var: &str) {
        self.ecs.append_to_world(filename, var);
    }

    pub fn run(mut self) {
        let event_loop = EventLoop::new();
        let title = env!("CARGO_PKG_NAME");
        let window = winit::window::WindowBuilder::new()
            .with_title(title)
            .with_transparent(false)
            .build(&event_loop)
            .unwrap();

        // create the rendering structure
        let mut state = block_on(State::new(&window, &self.ecs));

        let mut last_render_time = std::time::Instant::now();
        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;

            match event {
                Event::MainEventsCleared => window.request_redraw(),

                Event::DeviceEvent {
                    event:
                        DeviceEvent::Key(KeyboardInput {
                            virtual_keycode: Some(key),
                            state: kstate,
                            ..
                        }),
                    ..
                } => {
                    state.camera_controller.process_keyboard(key, kstate);
                }

                Event::DeviceEvent {
                    event: DeviceEvent::MouseMotion { delta },
                    ..
                } => {
                    state.camera_controller.process_mouse(delta.0, delta.1);
                }

                Event::WindowEvent {
                    ref event,
                    window_id,
                } if window_id == window.id() => {
                    //if !state.window_input(event) {
                    if true {
                        match event {
                            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                            WindowEvent::KeyboardInput { input, .. } => match input {
                                KeyboardInput {
                                    state: ElementState::Pressed,
                                    virtual_keycode: Some(VirtualKeyCode::Escape),
                                    ..
                                } => {
                                    *control_flow = ControlFlow::Exit;
                                }
                                KeyboardInput {
                                    state: kstate,
                                    virtual_keycode: Some(key),
                                    ..
                                } => {
                                    state.camera_controller.process_keyboard(*key, *kstate);
                                }
                                _ => {}
                            },
                            WindowEvent::Resized(physical_size) => {
                                state.resize(*physical_size);
                            }
                            WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                                state.resize(**new_inner_size);
                            }
                            _ => {}
                        }
                    }
                }

                Event::RedrawRequested(_) => {
                    let now = std::time::Instant::now();
                    let dt = now - last_render_time;
                    last_render_time = now;
                    state.update(dt);

                    // run updating methods
                    self.update_fps_ticker(now);

                    match state.render() {
                        Ok(_) => {}
                        Err(wgpu::SwapChainError::Lost) => state.resize(state.size),
                        Err(wgpu::SwapChainError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                        Err(e) => eprintln!("{:?}", e),
                    }
                }
                _ => {}
            }
        });
    }
}
