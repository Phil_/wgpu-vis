#![allow(unused_imports)]

mod app;
use app::App;

use legion::IntoQuery;
use vis_data::ecs::{Ecs, Position};
use vis_data::geo::{Coord, LambertConformal, Projection};

#[macro_use]
extern crate log;

fn main() {
    env_logger::init();

    //let mut ecs = ECS::new();
    //ecs.append_to_world("./test.nc", "CO");
    //let projection = ecs.resources.get::<Box<dyn Projection>>().unwrap();
    //let mut query = <&Position>::query();
    //let pos = query.iter(&ecs.world).next().unwrap();
    //let new_coord = projection.transform(&pos.coord);
    //dbg!(&pos.coord);
    //dbg!(new_coord);

    // construct the app
    let mut vis = App::new();
    vis.load_file("test.nc", "CO");
    info!("loaded app");
    vis.run();
}
