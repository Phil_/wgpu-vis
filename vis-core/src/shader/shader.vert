#version 450

layout(location=0) in vec3 a_position;
layout(location=1) in float a_value;

layout(location=0) out float v_value;

layout(set = 0, binding = 0)
uniform Uniforms {
    mat4 u_view_proj;
};

const float PI = 3.1415926535897932384626433832795;
const float PI_2 = 1.57079632679489661923;
const float PI_4 = 0.785398163397448309616;
const float R = 6371000;


// ------------------------

float cot(float x) {
    return cos(x) / sin(x);
}

float deg(float x) {
    return x * PI / 180;
}

vec2 transform(vec2 coord_in) {
    float phi1 = deg(30);
    float phi2 = deg(60);
    float lat_project_orig = deg(54.000004);
    float long_central_meridian = deg(12.5);

    float lon = deg(coord_in.x);
    float lat = deg(coord_in.y);

    float n_upper = log(cos(phi1) / cos(phi2));
    float n_lower = log(tan(PI_4 + 0.5 * phi2) * cot(PI_4 + 0.5 * phi1));
    float n = n_upper / n_lower;

    float F = cos(phi1) * pow(tan(PI_4 + 0.5 * phi1), n) / n;
    float rho  = R * F * pow(cot(PI_4 + 0.5 * lat), n);
    float rho0 = R * F * pow(cot(PI_4 + 0.5 * lat_project_orig), n);

    float x = rho * sin(n * (lon - long_central_meridian));
    float y = rho0 - rho * cos(n * (lon - long_central_meridian));
    return vec2(x, y);
}

void main() {
    v_value = a_value;

    vec2 coord = transform(a_position.xy) / 3000000;
    //gl_Position = vec4(coord, a_position.z, 1.0);
    gl_Position = u_view_proj * vec4(coord.xy, a_position.z, 1.0);
    //gl_Position = u_view_proj * vec4(a_position.z, coord.yx, 1.0);
}
